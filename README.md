<h2>Тестовый модуль для вывода данных в формате JSON (REST API)</h2>

<h3>Что делает установкой:</h3>
<ul>
<li>создает раздел /api/ с инициализацией роутинга модуля</li>
<li>добавляет правило в urlrewrite.php для обработки обращений к API</li>
<li>создает таблицу api_table и наполняет ее тестовыми данными</li>
</ul>
<h3>Endpoints для работы с таблицей:</h3>
<ul>
<li>список всех элементов /api/address/list/</li>
<li>получить элемент по id /api/address/get/$id/</li>
<li>добавить новый элемент /api/address/add/<br>
    тип запроса POST, параметры <i>name</i> и <i>address</i></li>
<li>обновить элемент /api/address/update/<br>
   тип запроса POST, параметры <i>id</i>, <i>name</i> и <i>address</i></li>
<li>удалить элемент /api/address/delete/$id/<br>
   тип запроса DELETE</li>
</ul>  