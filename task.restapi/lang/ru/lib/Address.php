<?php
$MESS['ELEMENTS_NOT_FOUND'] = 'Элементы не найдены';
$MESS['NOT_FOUND'] = 'Элемент не найден';
$MESS['ADDED_SUCCESS'] = 'Элемент успешно добавлен';
$MESS['ADDED_ERROR'] = 'Ошибка добавления:';
$MESS['ADDED_ERROR_NO_FIELDS'] = 'Ошибка добавления: не переданы обязательные поля';
$MESS['ADDED_ERROR_ELEMENT_EXIST'] = 'Ошибка добавления: элемент с параметрами уже существует';
$MESS['UPDATE_SUCCESS'] = 'Элемент успешно обновлен';
$MESS['UPDATE_ERROR'] = 'Ошибка обновления:';
$MESS['UPDATE_ERROR_NOT_FOUND'] = 'Ошибка обновления: элемент не найден';
$MESS['DELETE_SUCCESS'] = 'Элемент успешно удален';
$MESS['DELETE_ERROR'] = 'Ошибка удаления:';
$MESS['DELETE_ERROR_NOT_FOUND'] = 'Ошибка удаления: элемент не найден';