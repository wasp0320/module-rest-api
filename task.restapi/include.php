<?php

use Task\Restapi\Address;
use Task\Restapi\Router;

Bitrix\Main\Loader::registerAutoloadClasses(
    'task.restapi',
    [
        Router::class => 'lib/Router.php',
        Address::class => 'lib/Address.php',
    ]
);