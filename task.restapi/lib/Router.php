<?php

namespace Task\Restapi;

use ArgumentCountError;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Exception;

/**
 * Class Router
 * @package Task\Restapi
 */
class Router
{
    /**
     * Инициализирует роутинг для API
     */
    public function init()
    {
        $routes = $this->getRouters();

        $request = Application::getInstance()->getContext()->getRequest();
        $uriString = $request->getRequestUri();

        foreach ($routes as $key => $value) {
            if (strpos($uriString, $key) !== 0) {
                continue;
            }
            $class = $value['class'];
            $method = $value['method'];
        }


        if (!class_exists(__NAMESPACE__ . '\\' . $class) || !method_exists(__NAMESPACE__ . '\\' . $class, $method)) {
            return Loc::getMessage('WRONG_ENDPOINT');
        }

        $controllerClassName = __NAMESPACE__ . '\\' . $class;
        $controller = new $controllerClassName;
        $uriArray = explode('/', $uriString);
        $param = $uriArray[4];

        try {
            return $controller->{$method}($param);
        } catch (Exception | ArgumentCountError $e) {
            return Loc::getMessage('WRONG_ENDPOINT');
        }
    }

    /**
     * Возвращает список endpoints
     *
     * @return array
     */
    public function getRouters(): array
    {
        return [
            '/api/address/list/' => ['class' => 'Address', 'method' => 'getAll'],
            '/api/address/get/' => ['class' => 'Address', 'method' => 'getById'],
            '/api/address/update/' => ['class' => 'Address', 'method' => 'update'],
            '/api/address/add/' => ['class' => 'Address', 'method' => 'add'],
            '/api/address/delete/' => ['class' => 'Address', 'method' => 'delete'],
        ];
    }
}