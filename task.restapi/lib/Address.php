<?php

namespace Task\Restapi;

use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Localization\Loc;
use Exception;

/**
 * Class Address
 * @package Task\Restapi
 */
class Address
{
    /**
     * Возвращает список элементов
     *
     * @param $filter
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function getAll($filter = []): array
    {
        $params = [
            'select' => ['*'],
            'cache' => ['ttl' => 3600],
        ];

        if (!empty($filter) && is_array($filter)) {
            $params['filter'] = $filter;
        }

        $items = [];
        $result = AddressTable::getList($params);
        while ($row = $result->fetch()) {
            $row['CREATED_AT'] = $row['CREATED_AT']->toString();
            $row['UPDATED_AT'] = $row['UPDATED_AT']->toString();
            $items[] = $row;
        }

        if (empty($items)) {
            return self::getResponse('error', Loc::getMessage('ELEMENTS_NOT_FOUND'));
        }

        return self::getResponse('success', $items);
    }

    /**
     * Возвращает элемент по ID
     *
     * @param $id
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function getById($id): array
    {
        $result = AddressTable::getById($id);

        if ($row = $result->fetch()) {
            $row['CREATED_AT'] = $row['CREATED_AT']->toString();
            $row['UPDATED_AT'] = $row['UPDATED_AT']->toString();

            return self::getResponse('success', $row);
        }

        return self::getResponse('error', Loc::getMessage('NOT_FOUND'));
    }

    /**
     * Добавляет новый элемент
     *
     * @return array
     * @throws Exception
     */
    public static function add(): array
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $name = $request->getPost('name');
        $address = $request->getPost('address');

        if (empty($name) || empty($address)) {
            return self::getResponse('error', Loc::getMessage('ADDED_ERROR_NO_FIELDS'));
        }

        $curElements = self::getAll(['NAME' => $name, 'ADDRESS' => $address]);

        if ($curElements['status'] === 'success') {
            return self::getResponse('error', Loc::getMessage('ADDED_ERROR_ELEMENT_EXIST'));
        }

        $fields = [
            'NAME' => $name,
            'ADDRESS' => $address
        ];
        $result = AddressTable::add($fields);
        $id = $result->getId();

        if (empty($id)) {
            return self::getResponse('error', Loc::getMessage('ADDED_ERROR') . ' ' . $result->getErrorMessages());
        }

        return self::getResponse('success', Loc::getMessage('ADDED_SUCCESS'));
    }

    /**
     * Обновляет элемент по ID
     *
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws Exception
     */
    public static function update(): array
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $id = $request->getPost('id');
        $name = $request->getPost('name');
        $address = $request->getPost('address');

        $curElement = self::getById($id);

        if ($curElement['status'] === 'error') {
            return self::getResponse('error', Loc::getMessage('UPDATE_ERROR_NOT_FOUND'));
        }

        $fields = [
            'NAME' => (!empty($name)) ? $name : $curElement['NAME'],
            'ADDRESS' => (!empty($address)) ? $address : $curElement['ADDRESS'],
            'UPDATED_AT' => DateTime::createFromTimestamp(time())
        ];

        $result = AddressTable::update($id, $fields);

        if ($result->isSuccess()) {
            return self::getResponse('success', Loc::getMessage('UPDATE_SUCCESS'));
        }

        return self::getResponse('error', Loc::getMessage('UPDATE_ERROR') . ' ' . $result->getErrorMessages());
    }

    /**
     * Удаляет элемент по ID
     *
     * @param $id
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws Exception
     */
    public static function delete($id): array
    {
        $curElement = self::getById($id);

        if ($curElement['status'] === 'error') {
            return self::getResponse('error', Loc::getMessage('DELETE_ERROR_NOT_FOUND'));
        }

        $result = AddressTable::delete($id);

        if ($result->isSuccess()) {
            return self::getResponse('success', Loc::getMessage('DELETE_SUCCESS'));
        }

        return self::getResponse('error', Loc::getMessage('DELETE_ERROR') . ' ' . $result->getErrorMessages());
    }

    /**
     * Формирует ответ
     *
     * @param $status
     * @param $data
     * @return array
     */
    private static function getResponse($status, $data): array
    {
        return [
            'status' => $status,
            'data' => $data
        ];
    }
}