<?php

namespace Task\Restapi;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

/**
 * Class AddressTable
 *
 * Поля:
 *
 * ID int
 * NAME string
 * ADDRESS string
 * CREATED_AT datetime default 'CURRENT_TIMESTAMP'
 * UPDATED_AT datetime default 'CURRENT_TIMESTAMP'
 *
 *
 * @package \Task\Restapi
 **/
class AddressTable extends Entity\DataManager
{
    /**
     * Возвращает название таблицы в БД для сущности
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'api_table';
    }

    /**
     * Возвращает массив, описывающий поля сущности
     * @return array
     */
    public static function getMap(): array
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('DATA_ENTITY_ID_FIELD'),
            ],
            'NAME' => [
                'data_type' => 'text',
                'required' => true,
                'title' => Loc::getMessage('DATA_ENTITY_NAME_FIELD'),
            ],
            'ADDRESS' => [
                'data_type' => 'text',
                'title' => Loc::getMessage('DATA_ENTITY_ADDRESS_FIELD'),
            ],
            'CREATED_AT' => [
                'data_type' => 'datetime',
                'title' => Loc::getMessage('DATA_ENTITY_CREATED_FIELD'),
            ],
            'UPDATED_AT' => [
                'data_type' => 'datetime',
                'title' => Loc::getMessage('DATA_ENTITY_UPDATED_FIELD'),
            ],
        ];
    }
}