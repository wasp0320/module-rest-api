<?php

use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\UrlRewriter;
use Bitrix\Main\Localization\Loc;

class Task_restapi extends CModule
{
    public $MODULE_ID = 'task.restapi';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $errors;

    public function __construct()
    {
        $this->MODULE_VERSION = '1.0.0';
        $this->MODULE_VERSION_DATE = '08.07.2022';
        $this->MODULE_NAME = Loc::getMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_DESCRIPTION');
    }

    /**
     * Установка модуля
     * @throws ArgumentNullException
     */
    public function DoInstall()
    {
        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();
        $this->InstallUrlRewrite();

        RegisterModule($this->MODULE_ID);

        return true;
    }

    /**
     * Удаление модуля
     * @throws ArgumentNullException
     */
    public function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UninstallUrlRewrite();

        UnRegisterModule($this->MODULE_ID);

        return true;
    }

    /**
     * Выполнение SQL запросов модуля
     */
    public function InstallDB()
    {
        global $DB;

        $this->errors[] = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/db/install.sql');
        $this->errors[] = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/db/insert.sql');

        return !empty($this->errors) ? $this->errors : true;
    }

    /**
     * Откат SQL запросов модуля
     */
    public function UnInstallDB()
    {
        global $DB;
        $this->errors[] = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/db/uninstall.sql');

        return !empty($this->errors) ? $this->errors : true;
    }

    /**
     * Установка событий модуля
     */
    public function InstallEvents()
    {
        return true;
    }

    /**
     * Удаление событий модуля
     */
    public function UnInstallEvents()
    {
        return true;
    }

    /**
     * Установка файлов модуля
     */
    public function InstallFiles()
    {
        CopyDirFiles(
            $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/section/',
            $_SERVER['DOCUMENT_ROOT'] . '/',
            true,
            true
        );

        return true;
    }

    /**
     * Удаление файлов модуля
     */
    public function UnInstallFiles()
    {
        DeleteDirFilesEx('/api');

        return true;
    }

    /**
     * Добавление правила в urlrewrite
     *
     * @throws ArgumentNullException
     */
    public function InstallUrlRewrite(): void
    {
        $siteId = CSite::GetDefSite();
        UrlRewriter::add(
            $siteId,
            [
                'CONDITION' => '#^/api/#',
                'ID' => 'task.restapi',
                'PATH' => '/api/index.php',
                'RULE' => ''
            ]
        );
    }

    /**
     * Удаление правила из urlrewrite
     *
     * @throws ArgumentNullException
     */
    public function UninstallUrlRewrite(): void
    {
        $siteId = CSite::GetDefSite();
        UrlRewriter::delete(
            $siteId,
            [
                'ID' => 'task.restapi'
            ]
        );
    }
}