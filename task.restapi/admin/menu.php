<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$aMenu = array(
    array(
        'parent_menu' => 'global_menu_content',
        'sort' => 400,
        'text' => Loc::getMessage("MENU_TITLE"),
        'title' => '',
        'url' => 'perfmon_table.php?lang=ru&table_name=api_table',
        'items_id' => 'menu_references'
    )
);

return $aMenu;